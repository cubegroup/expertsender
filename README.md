**Instalacja**: `npm install git+ssh://git@bitbucket.org:cubegroup/expertsender.git`


**Inicjalizacja**: `const Expertsender = require('expertsender')(API_KEY)`;


**Dostępne metody**:

1. `requestES(url, method, content, callback, responseType)`
2. `addToBlackList(userData, callback)`
3. `addUserContact(userData, callback)`
4. `searchUserContactByEmail(userData, callback)`
5. `mapCustomFields(callback)`