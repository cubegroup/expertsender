const https = require('https');
const parseStringXML = require('xml2js').parseString;
const csv = require('csvtojson');

const parseStringCSV = (csvFilePath, callback) => {
  const csvData = [];
  const csvInstance = csv({
    delimiter: ',',
  });
  csvInstance.fromString(csvFilePath)
    .on('json', (jsonObj) => {
    csvData.push(jsonObj);
})
.on('done', (error) => {
    if (error) {
      console.error(error);
    } else {
      callback(error, csvData);
}
});
};

const Expertsender = (key) => {
  const API_KEY = key;

  const makeRequestES = (url, method, content, callback, responseType) => {
    let data = '';
    const headers = { Accept: '*/*' };
    if (content) {
      headers['Content-Length'] = Buffer.byteLength(content);
    }
    const path = `/v2/Api/${url}`;
    const request = https.request({
      host: 'api5.esv2.com',
      port: '443',
      path,
      method,
      headers,
    }, (res) => {
      res.setEncoding('utf8');
    res.on('data', (chunk) => {
      data += chunk;
  });
    res.on('error', (err) => {
      callback(err);
  });
    res.on('end', () => {
      if (responseType !== 'CSV') {
      parseStringXML(data, (err, result) => {
        callback(null, result);
    });
    } else if (data[0] === '<') {
      callback(null, []);
    } else {
      parseStringCSV(data, (err, result) => {
        callback(null, result);
    });
    }
  });
  });
    if (content) {
      request.write(content);
    }
    request.on('error', (err) => {
      console.error(err.message);
    setTimeout(() => {
      makeRequestES(url, method, content, callback);
  }, 1000);
  });
    request.end();
  };

  const setCustomFields = (data) => {
    let string = '';
    if (data.customFields) {
      string += '<Properties>';
      for (const key in data.customFields) {
        string += `<Property>
          <Id>${data.customFields[key].id}</Id>
          <Value xsi:type="xs:${data.customFields[key].type}"${!data.customFields[key].value ? ' xsi:nil="true"' : ''}>${data.customFields[key].value}</Value>
        </Property>`;
      }
      string += '</Properties>';
    }
    return string;
  };

  return {
    requestES: (url, method, content, callback, responseType) => {
    makeRequestES(url, method, content, callback, responseType)
  },
    addToBlackList: (userData, callback) => {
    makeRequestES(`Subscribers?apiKey=${API_KEY}&email=${encodeURIComponent(userData.email)}&option=Short`, 'DELETE', '', (err, data) => {
      callback(null);
  });
  },
  /**
   * Usuniecie klienta z ES
   * @param {string} id Custom Subscriber Id
   * @param {function} callback
   */
  removeUserContact: (id, listId, callback) => {
    makeRequestES('Subscribers?customSubscriberId='+id+'&apiKey=' + API_KEY + (listId ? '&listId=' + listId : ''), 'DELETE', '', (err, data) => {
      if (data && data.ApiResponse.ErrorMessage) {
      err = data.ApiResponse.ErrorMessage[0].Message[0];
    }
    callback(err);
  });
  },
  /**
   * Funkcja ma podwójne zastosowanie, z jednej strony dodaje lub updateuje właściwości kontaktu, z drugiej strony dodaje do listy
   * @param {Object} userData
   * @param {function} callback
   */
  addUserContact: (userData, callback) => {
    const email = userData.email ? `<Email>${userData.email}</Email>` : '';
    const firstName = userData.firstName ? `<Firstname>${userData.firstName}</Firstname>` : '';
    const lastName = userData.lastName ? `<Lastname>${userData.lastName}</Lastname>` : '';
    const customSubscriberId = userData.customSubscriberId ? `<CustomSubscriberId>${userData.customSubscriberId}</CustomSubscriberId>` : '';
    makeRequestES('Subscribers/', 'POST', `<ApiRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xs="http://www.w3.org/2001/XMLSchema">
        <ApiKey>${API_KEY}</ApiKey>
        <Data xsi:type="Subscriber">
          <Mode>AddAndUpdate</Mode>
          <Force>true</Force>
          <AllowRemoved>true</AllowRemoved>
          <AllowUnsubscribed>true</AllowUnsubscribed>
          <ListId>${userData.groupId}</ListId>
          <MatchingMode>${userData.matchingMode}</MatchingMode>
          ${customSubscriberId}
          ${email}
          ${firstName}
          ${lastName}
          ${setCustomFields(userData)}
        </Data>
      </ApiRequest>`, (err, data) => {
      if (data && data.ApiResponse.ErrorMessage) {
      err = data.ApiResponse.ErrorMessage[0].Message[0];
    }
    callback(err, userData);
  });
  },
  /**
   * Funkcja do triggerowania customowego eventu e ES.
   * @param {Object} userData
   * @param {Function} callback
   */
  workflowCustomEvents: (userData, callback) => {
    makeRequestES('WorkflowCustomEvents', 'POST', `<ApiRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xs="http://www.w3.org/2001/XMLSchema">
        <ApiKey>${API_KEY}</ApiKey>
        <Data>
          <CustomEventId>${userData.customEventId}</CustomEventId>
          <CustomSubscriberId>${userData.customSubscriberId}</CustomSubscriberId>
        </Data>
      </ApiRequest>`, (err, data) => {
      if (data && data.ApiResponse.ErrorMessage) {
      err = data.ApiResponse.ErrorMessage[0].Message[0];
    }
    callback(err, userData);
  });
  },
  /**
   * Funkcja wyszukuje kontakt w ExpertSenderze.
   *
   * @param {Object} userData
   * @param {Function} callback
   */
  searchUserContactByEmail: (userData, callback) => {
    makeRequestES(`Subscribers?apiKey=${API_KEY}&email=${encodeURIComponent(userData.email)}&option=Short`, 'GET', '', (err, data) => {
      callback(null, data.ApiResponse.Data && data.ApiResponse.Data[0].StateOnLists[0].StateOnList[0].Status.indexOf('Active') > -1 ? userData.email : '');
  });
  },
  /**
   * Pobranie Custom fields z ExpertSendera i zbudowanie mapy pól, która zawiera typ i id pola.
   * Mapa jest zwracana jako pierwszy argument callbacku.
   *
   * @param {string} apiKey - Klucz API do ExpertSendera.
   * @param {Function} callback - Dalszy kod wykonywany po zakońćzeniu funkcji.
   */
  mapCustomFields: (callback) => {
    const types = {
      Text: 'string',
      Boolean: 'boolean',
      Number: 'integer',
      Money: 'float',
      Date: 'dateTime',
    };
    console.log('Pobieranie listy pól');
    makeRequestES(`Fields?apiKey=${API_KEY}`, 'GET', '', (err, data) => {
      const fieldsMap = {};
    console.log('Pobrano pola');
    if (data.ApiResponse.Data[0].Fields[0]) {
      data.ApiResponse.Data[0].Fields[0].Field.forEach((item) => {
        fieldsMap[item.FriendlyName[0]] = { id: item.Id[0], type: types[item.Type[0]] };
    });
    }
    callback(fieldsMap);
  });
  }
}
};

module.exports = Expertsender;